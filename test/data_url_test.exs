defmodule DataUrlTest do
  use ExUnit.Case, async: true
  import DataUrl

  describe "parse/1" do
    test "with empty string returns error" do
      assert {:error, _reason} = parse("")
    end

    test "with invalid prefix returns error" do
      assert {:error, _reason} = parse("test:hello")
    end

    test "with simple text returns parsed url" do
      assert {:ok,
              %DataUrl{
                mime_type: "text/plain;charset=US-ASCII",
                data: "Hello, World!"
              }} = parse("data:,Hello%2c%20World!")
    end

    test "with html returns parsed url" do
      assert {:ok,
              %DataUrl{
                mime_type: "text/html",
                data: "<h1>Hello, World!</h1>"
              }} = parse("data:text/html,%3Ch1%3EHello%2C%20World!%3C%2Fh1%3E")
    end

    test "with base64 returns parsed url" do
      assert {:ok,
              %DataUrl{
                mime_type: "text/plain",
                data: "Hello, World!"
              }} = parse("data:text/plain;base64,SGVsbG8sIFdvcmxkIQ==")
    end

    test "with base64 and mime with option returns parsed url" do
      assert {:ok,
              %DataUrl{
                mime_type: "text/plain;charset=ISO-8859-1",
                data: "Hello, World!"
              }} = parse("data:text/plain;charset=ISO-8859-1;base64,SGVsbG8sIFdvcmxkIQ==")
    end

    test "with invalid base64 returns error" do
      assert {:error, _} = parse("data:;base64,=")
    end

    test "with base64 percent sign returns percent sign" do
      assert {:ok,
              %DataUrl{
                mime_type: "text/plain;charset=US-ASCII",
                data: "%"
              }} = parse("data:;base64,JQ==")
    end
  end
end
