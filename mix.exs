defmodule DataUrl.MixProject do
  use Mix.Project

  def project do
    [
      app: :data_url,
      deps: deps(),
      description: "Parse data: URLs and access the data contained therein",
      docs: docs(),
      elixir: "~> 1.10",
      homepage_url: "https://gitlab.com/ericlathrop/data_url",
      package: package(),
      source_url: "https://gitlab.com/ericlathrop/data_url",
      start_permanent: Mix.env() == :prod,
      version: "0.1.0"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      # extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.23", only: :dev, runtime: false}
    ]
  end

  defp docs do
    [
      main: "readme",
      extras: ["README.md", "CHANGELOG.md"]
    ]
  end

  defp package do
    [
      licenses: ["MIT"],
      links: %{
        "GitLab" => "https://gitlab.com/ericlathrop/data_url"
      }
    ]
  end
end
