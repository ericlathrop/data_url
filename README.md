# data_url

[Hex.pm](https://hex.pm/packages/data_url) [Documentation](https://hexdocs.pm/data_url/readme.html)

Parse data: URLs and access the data contained therein

## Usage

```elixir
iex(1)> DataUrl.parse("data:text/plain;charset=ISO-8859-1;base64,SGVsbG8sIFdvcmxkIQ==")
{:ok,
 %DataUrl{data: "Hello, World!", mime_type: "text/plain;charset=ISO-8859-1"}}
```

## Installation

The package can be installed by adding `data_url` to your list of dependencies
in `mix.exs`:

```elixir
def deps do
  [
    {:data_url, "~> 0.1.0"}
  ]
end
```
